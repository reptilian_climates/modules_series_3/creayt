


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'structures_pip'
])

import BODY_SCAN

import pathlib
from os.path import dirname, join, normpath

this_folder = pathlib.Path (__file__).parent.resolve ()
structures = normpath (join (this_folder, "../structures/modules"))

SCAN = BODY_SCAN.START (
	# REQUIRED
	GLOB = structures + '/**/status_*.py',
	
	SIMULTANEOUS = True,
	
	# OPTIONAL
	MODULE_PATHS = [
		structures
	],
	
	# OPTIONAL
	RELATIVE_PATH = structures
)



#
#
#